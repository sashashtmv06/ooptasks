package oopTasks.oneTask;

public class SamsungPhone extends Phone implements PhoneMedia {
    public SamsungPhone(String name, String model, int dataHdd, int dataRam) {
        super(name, model, dataHdd, dataRam);
    }

    @Override
    public void call() {
        System.out.println(String.format("Дзвінок відбувся з телефону під назвою - %s, моделі - %s, з внутрішнім сховищем - %d та ОЗУ - %d", getName(), getModel(), getDataHdd(), getDataRam()));
    }

    @Override
    public void sendMessage() {
        System.out.println(String.format("Повідомлення відправлене з телефону під назвою - %s, моделі - %s, з внутрішнім сховищем - %d та ОЗУ - %d", getName(), getModel(), getDataHdd(), getDataRam()));
    }

    @Override
    public void makePhoto() {
        System.out.println(String.format("Фото зроблене з телефону під назвою - %s, моделі - %s, з внутрішнім сховищем - %d та ОЗУ - %d", getName(), getModel(), getDataHdd(), getDataRam()));
    }

    @Override
    public void makeVideo() {
        System.out.println(String.format("Відео записане з телефону під назвою - %s, моделі - %s, з внутрішнім сховищем - %d та ОЗУ - %d", getName(), getModel(), getDataHdd(), getDataRam()));
    }
}
