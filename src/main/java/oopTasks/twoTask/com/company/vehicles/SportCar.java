package oopTasks.twoTask.com.company.vehicles;

import oopTasks.twoTask.com.company.details.Engine;
import oopTasks.twoTask.com.company.professions.Driver;

public class SportCar extends Car{
    private int limitSpeed;

    @Override
    public void start() {
        System.out.println("Розгоняємо нашого монстра");
    }

    @Override
    public void stop() {
        System.out.println("Спробую зупинити цю шалену швидкість");
    }

    @Override
    public void turnRight() {
        System.out.println("Поворот обережно направо, дріфт ще не на часі");
    }

    @Override
    public void turnLeft() {
        System.out.println("Поворот обережно наліво, дріфт ще не на часі");
    }

    public int getLimitSpeed() {
        return limitSpeed;
    }

    public void setLimitSpeed(int limitSpeed) {
        this.limitSpeed = limitSpeed;
    }

    public SportCar(String carModel, String classModel, int weight, Driver driver, Engine engine, int limitSpeed) {
        super(carModel, classModel, weight, driver, engine);
        this.limitSpeed = limitSpeed;
    }
}
