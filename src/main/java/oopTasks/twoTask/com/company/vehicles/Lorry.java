package oopTasks.twoTask.com.company.vehicles;

import oopTasks.twoTask.com.company.details.Engine;
import oopTasks.twoTask.com.company.professions.Driver;

public class Lorry extends Car {
    @Override
    public void start() {
        System.out.println("Тягну вантаж на лорі");
    }

    @Override
    public void stop() {
        System.out.println("Зупиняємо свою вантажівочку");
    }

    @Override
    public void turnRight() {
        System.out.println("Поворот обережно направо, вантаж можна загубити");
    }

    @Override
    public void turnLeft() {
        System.out.println("Поворот обережно наліво, вантаж можна загубити");
    }

    private String loadCapacity;

    public String getLoadCapacity() {
        return loadCapacity;
    }

    public void setLoadCapacity(String loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    public Lorry(String carModel, String classModel, int weight, Driver driver, Engine engine, String loadCapacity) {
        super(carModel, classModel, weight, driver, engine);
        this.loadCapacity = loadCapacity;
    }
}
